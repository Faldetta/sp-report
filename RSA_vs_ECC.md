---
title: Da RSA a ECC
author: Filippo Faldetta
toc: true
numbersections: true
toc-depth: 5
---

\newpage

Introduzione
============

Quando parliamo di crittografia possiamo chiaramente distinguere due
epoche distinte: la prima è quella della crittografia simmetrica, dove,
stabilito un algoritmo, entrambe le parti utilizzano la medesima chiave;
la seconda è l\'epoca della crittografia asimmetrica, dove ogni utente
non ha più una sola chiave, bensì due, una pubblica da distribuire ed
una privata da mantenere segreta. Si noti che non vogliamo insinuare in
nessun modo che la crittografia simmetrica sia superata, infatti mentre
quella assimmetrica risulta più efficente dal punto di vista della
gestione delle chiavi in un sistema complesso, tale efficenza viene meno
quando si parla di velocità di calcolo, dove la crittografia simmetrica
fa da padrona. È proprio per questo motivo che queste famiglie di
algoritmi vengono utilizzate insieme (ad esempio possiamo usare la
crittografia asimmetrica per scambiare o concordare una chiave
simmetrica da usare per le successive comunicazioni).

I crittosistemi a chiave asimmetrica nascono tra il 1970 e il 1973,
possiamo dire che in questi anni è avvenuta la prima grande rivoluzione
nel mondo della crittografia. In questi anni James H. Ellis teorizzò la
possibilità di usare una \"chiave non segreta\" e il team composto da
Ron Rivest, Adi Shamir e Leonard Adleman realizzò tale sistema, che
conosciamo con l\'acronimo dei loro cognomi: RSA.

Tuttavia più il tempo passa, più i computer diventano potenti, più gli
algoritmi come RSA perdono di efficenza. Non a caso, già nel 2016,
l\'NSA consigliava di usare per tale crittosistema chiavi lunghe almeno
$3072$ bit (e se lo dice l\'NSA, probabilmente, è ancora troppo corta).

Per fortuna, nel 1985, Neal Koblitz e Victor S. Milller suggeriscono
(indipendentemente) l\'uso delle curve ellittice per la realizzazione di
un crittosistema. Questa famigia di algoritmi, più efficenti di RSA,
iniziano a diffondersi nel 2004-2005.

Vediamo brevemente il funzionamento di questi crittosistemi per poi
concludere effettuando un confronto.

\newpage

RSA
===

Descriviamo ora come opera il crittosistema RSA.

Funzionamento
-------------

Ipotiziamo di avere un messaggio $M$ da voler cifrare:

1.  convertiamo il messaggio in un vettore di numeri di lunghezza $k$
    che chiameremo $m$
2.  scegliamo due numeri primi casuali $p$ e $q$ molto grandi (la loro
    dimensione ci fornisce sicurezza) e calcoliamo $n=pq$
3.  calcoliamo $\Phi(n) = \Phi(pq)$ e poichè $p$ e $q$ sono primi
    $\Phi(pq)=(p-1)(q-1)$
4.  randomicamente scegliamo un numero $e$ primo con $\Phi(n)$, quindi
    abbiamo:
    -   la **chiave pubblica** è definita dalla coppia $(n,e)$
    -   la **chiave privata** è il numero $d$, inverso di $e$ in
        $\mathbb{Z}_{\Phi(n)}$ che realizza l\'equivalenza
        $ed \equiv 1 \thinspace mod(\Phi(n))$
    -   effettuata tale procedura, i numeri $p$, $q$, e $\Phi(n)$
        diventano inutili, possono quindi essere distrutti (o
        dimenticati)
5.  per cifrare il messaggio $m$, ottenendo così il *cyphertext* $c$,
    basterà calcolare $c=m^e mod(n)$
    -   si osservi che basta sapere la chiave pubblica $e$ del
        destinatario del messaggio $m$
6.  per decifrare $c$, il destinatario, dovrà usare la sua chiave
    privata $d$ per calcolare
    $c^d mod(n)=(m^e)^d mod(n)=m^{ed}mod(n)=m mod(n)$
    -   questo è possibile poichè $ed \equiv 1 mod((p-1)(q-1))$ da cui
        deriva che $ed \equiv 
         1 mod(p-1)$ e $ed \equiv 1 mod(q-1)$
    -   quindi per il *Piccolo Teorema di Fermat*
        $m^{ed} \equiv m mod(p)$ e $m^{ed} \equiv 
         m mod(q)$
    -   quindi essendo $p \neq q$, con $p$ e $q$ primi, applicando il
        *Teorema Cinese del Resto*, possiamo affermare che
        $m^{ed}\equiv m mod(pq)$ e quindi che $c^d \equiv m mod(n)$
7.  riconvertiamo il messaggio $m$ nella sua forma testuale $M$

Alle stesse conclusioni è possibile arrivare tramite il *Teorema di
Eulero*: possiamo affermare che $ed=1+h\Phi(n)$ per qualche $h$ non
negativo. Assumendo che $m$ sia primo con $n$ abbiamo che
$m^{ed}mod(n)=m^{1+h\Phi(n)}mod(n) = m(m^{\Phi(n)})^h mod(n) \equiv m(1)^h mod(n) \equiv m mod(n)$.

Vulnerabilità
-------------

RSA possiede delle vulnerabilità che possono dare adito ad attacchi.
Queste vulnerabilità non sempre dipendono dalla teoria che abbiamo
mostrato in precedenza, anzi, spesso la colpa è dell\' implementazione o
di eventuali ottimizzazioni che forniscono all\'attacante informazioni
utili per rompere la crittografia. Non rientra nello scopo di questo
documento parlare di tale famiglia di vulnerabilità.

L\'unica vulnerabilità strettamente legata all\'argomento che vogliamo
trattare riguarda il problema utilizzato da RSA e la lunghezza delle sue
chiavi.

### Fattorizzazone in Numeri Primi

La sicurezza di RSA risiede nella difficoltà di fattorizzare il numero
$n$ nei suoi fattori primi $p$ e $q$, dai quali si può derivare la
chiave privata $d$ a partire da quella pubblica $e$.

Fortunatamente non esiste un metodo per fattorizzare un numero molto
grande nei suoi fattori primi in tempo polinomiale, difatti l\'esistenza
di un tale algoritmo non è stata dimostrata. Tale problema appartiene
alla classe $NP$, tuttavia non si sa se è $NP\textrm{-completo}$.

La seguente tabella può dare un\'idea della crescita del tempo e dello
spazio richiesti per la fattorizzazione di un numero in funzione della
sua lunghezza in bit:

  Bit   Tempo         Memoria
  ----- ------------- ----------
  128   0.4886 s      0.1 MiB
  192   3.9979 s      0.5 MiB
  256   103.1746 s    3 MiB
  300   1175.7826 s   10.9 Mib

Volendo dare qualche altro esempio di fattorizzazioni:

-   nel 2009 una chiave di 512 bit è stata fattorizzata in 73 giorni
    utilizzando 2.5 GB di RAM;
-   nel 2019 il più grande numero fattorizzato era lungo 795 bit
    (RSA-240) e sono stati impiegati 4000 \"anni-core\" (1 anno-core
    $\equiv$ esecuzione di un core per un anno $\equiv$ esecuzione di 12
    core per un mese $\equiv$ esecuzione di 365 core per un giorno)
    utilizzando un processore di \"fascia alta\" pensato per server e
    workstation.

Da questi valori si evince che il problema è estremamente oneroso in
termini di risorse, ma si può trarre anche la conclusione che con il
passare degli anni le chiavi di RSA dovranno essere sempre più lunghe;
gia oggi giorno le chiavi sono lunghe dai 1024 ai 4096 bit, il valore
raccomandato è almeno 2048 bit.

Per quanto riguarda la fattorizzazione RSA ha un\'altra minaccia, ovvero
i computer quantistici, infatti nel 1994, Peter Shor ha mostrato che un
calcolatore di questo tipo sarebbe in grado di fattorizare un numero in
fattori primi in tempo polinomiale.

\newpage

ECC
===

Prima di descrivere il funzionamento della crittografia a curve
ellittiche, è necessario introdurre quest\'ultime.

Introduzione alle Curve Ellittiche
----------------------------------

Una curva ellittica $E$ su un campo $\mathbb{K}$ è una curva non
singolare di terzo grado nel piano proiettivo, formata dall\'insieme
delle coppie $(x,y) \in \mathbb{K}^2$ che soddisfano l\'equazione
$$ f(x,y)=y^2 + a_1xy+a_3y-x^3-a_2x^2-a_4-a_6=0 $$ con $a_i \in k$, più
un punto all\'infinito $\mathcal{O}$.

Per quanto riguarda la crittografia si usano curve ellittiche su un
campo $\mathbb{K}$ con caratteristica maggiore di 3; in questo caso
l\'equazione che abbiamo appena mostrato degenera in $$y^2=x^3+ax+b$$
con $a,b \in \mathbb{K}$ e con $x^3+ax+b$ che non ha radici multiple, in
modo che la curva sia liscia, più il punto all\'infinito $\mathcal{O}$.

Prima di procedere dobbiamo fornire $E$ di una struttura di gruppo
abeliano, definiamo quindi un\'operazione di somma tra punti (a volte
chiamata *\"dot\"*) con le seguenti proprietà:

\renewcommand{\labelitemii}{$\star$}

-   il punto all\'infinito $\mathcal{O}$ è l\'elemento neutro, quindi
    -   $-\mathcal{O}=\mathcal{O}$
    -   $P+\mathcal{O}=\mathcal{O}+P=P$
-   se $P\neq \mathcal{O}$, allora $-P$ è l\'unico altro punto della
    curva, diverso dal punto all\' infinito, con la stessa ascissa di
    $P$
-   se $P$ e $Q$ hanno ascisse diverse, $P+Q$ si ottiene tracciano la
    retta $t$ che li congiunge e prendendo l\'intersezione di $t$ con
    $E$, chiamiamola $R$, di tale punto prendiamo l\'inverso (come visto
    nella proprietà precedente, quindi $P+Q=-R$; è stato dimostrato che
    questo punto esiste ed è unico
-   se $Q=-P$, allora $P+Q=\mathcal{O}$
-   se $Q=P$, per calcolare la somma $P+Q$, si traccia la retta $t$
    tangente ad $E$ in $P$, trovando così il punto di intersezione $R$
    con $E$, ancora una volta, di tale punto prendiamo l\'inverso,
    quindi $P+Q=P+P=2P=-R$

\renewcommand{\labelitemii}{$-$}

Alla luce dalle proprietà appena elencate possiamo definire, data una
curva ellittica $E$, un punto $P$ su di essa e un numero
$k \in \mathbb{Z}$, il multiplo $kP$ di un punto:

-   $kP=\mathcal{O}$ se $k=0$
-   $kP = P+\dots + P$ ($k$ volte), se $k>0$
-   $kP = (-P)+\dots + (-P)$ ($|k|$ volte), se $k<0$

Infine, per rendere le curve ellittiche idonee all\'applicazione
crittografica, è necessario spostarsi su un campo finito.

Quindi, sia $\mathbb{K}$ un campo finito $\mathbb{F}_q$, con $q=p^r$,
preso $p$ numero primo e $r\geq 1 \in \mathbb{N}$, sia $E$ una curva
ellittica su $\mathbb{K}$, si nota facilmente che $E$ può avere al più
$2q+1$ punti (2 per ogni valore $\leq q$ più il punto all\'infinito).
Tuttavia, siccome solo la metà degli elemeneti di $\mathbb{F}_q$ ha
radice quadrata si può pensare che questi siano \"quasi\" $q$ (in
particolare $q+1 \pm 2\sqrt{q}$).

Ora che abbiamo visto, oltre alla definizione della curva ellittica e
delle operazioni di somma tra punti (e l\'estensione di questa alla
moltiplicazione di un punto per un intero), che la curva ellittica può
essere discretizzata (quindi approssimata da punti discreti), possiamo
procedere alla descrizione del crittosistema associato a tali curve.

Crittografia con Curve Ellittiche
---------------------------------

La sicurezza della crittografia a curve ellittiche si basa su un
problema considerato più difficile della fattorizzazione in numeri
primi: il logaritmo discreto su una curva ellittica.

Tale problema è definito come segue: presa una curva ellittica $E$
definita su un campo finito $\mathbb{F}_q$, un punto $P \in 
E(\mathbb{F}_q)$ di ordine $n$ e un punto $Q=kP$, con
$k \in [0,\dots,n-1]$. Il numero intero $k$ è chiamato logaritmo
discreto di $Q$ in base $P$ e si indica con $k=\log_P Q$. Con ordine $n$
di un punto $P\in E$ si intende il più piccolo intero positivo tale che
$nP=\mathcal{O}$ (ma anche $knP=\mathcal{O}$ per ogni intero $k$).

Premesso ciò, un crittosistema basato su curve ellittiche è definito da:

-   $q$: ordine del campo finito $\mathbb{F}_q$
-   *field representation*: rappresentazione usata per gli elementi di
    $\mathbb{F}_q$
-   $S$: seme usato nel caso di generazione casuale di curve ellittiche
-   $a,b\in\mathbb{F}_q$: coefficenti dell\'equazione che descrive la
    curva
-   $P\in E(\mathbb{F}_q)$: punto base, da cui parte la \"ricerca della
    chiave\"
-   $n$: ordine del punto base, deve essere primo

Definiti i parametri sopra elencati possiamo passare alla generazione
delle chiavi (pubblica e privata):

1.  selezioniamo un numero $d\in[1, n-1]$ (questa sarà la nostra chiave
    privata)
2.  calcoliamo $Q=dP$ (questa sarà la nostra chiave pubblica)
3.  ritorniamo la coppia di chiavi (Q, d)

Possiamo vedere adesso come è possibile cifrare e decifrare un messaggio
al fine di ottenere la segretezza di quest\'ultimo. Premettiamo cha per
ottenere tale scopo ci sono molteplici tecniche, riportiamo di seguito
il metodo noto come *embedding* di Kobliz, nel quale un messaggio viene
rapppresentato come un punto sulla curva.

Supponiamo di avere una curva ellitica $E$ definita come $y^2=x^3+ax+b$
definita sul campo primo $\mathbb{F}_p$, con $p$ primo tale che
$p\equiv 3 mod(4)$.

1.  prendiamo un messaggio $M$, rappresentiamolo come un intero
    $0\leq m < p/100-1$
2.  definiamo $x_i=100m+i$ con $i=0,\dots,99$
3.  calcoliamo, per $i=0, \dots, 99$ i valori $s_i=x_i^3+ax_i+b$ fino a
    quando non otteniamo un quadrato modulo $p$
    -   si può dimostrare che la radice quadrata è facilmente
        ricavabile: $y_i \equiv s_i^{(p+1)/4} 
         mod(p)$

    otteniamo in questo modo il punto $P_m=(x_i, y_i)$ sulla curva $E$
4.  per decifrare il messaggio a partire dal punto $P_m$ è sufficiente
    calcolare il più piccolo intero minore o uguale di $x_i / 100$

Vediamo ora se e come è possibile attaccare questo crittosistema.

Vulnerabilità
-------------

Abbiamo detto che il problema alla base dell\'ECC è il logaritmo
discreto su curve ellittiche, gli attacchi, e le vulnerabilità, che
andremo a vedere cercano in qualche modo di aggirare il problema, o
comunque di limitarne la complessità.

Come spesso in questi casi, esiste un attacco \"naive\", la ricerca
esaustiva del logaritmo, tuttavia, come nel caso della fattorizzazione,
all\'aumentare della dimensione dei numeri il problema diventa
intrattabile (infatti nessuno ha dimostrato l\'esistenza di un algoritmo
efficente).

Escluso l\'\"attacco\" appena descritto rimango gli attacchi per curve
speciali, che si basano su \"vulnerabilità\" di alcune curve ellittiche,
e quelli generici che si basano sulla dimensione della curva.

### Attacchi Generici

#### $\rho$ di Pollard

Presi $P,Q \in E$, con $P$ di ordine $n$ tali che $hP=Q$. L\'obiettivo è
quello di trovare 4 interi $n_1,\dots,n_4$ tali che
$n_1P+n_2Q=n_3P+n_4Q$, in questo modo $h=(n_1-n_3)/(n_4-n_2) mod(n)$. I
quattro interi sono da cercare in un cammino pseudorandom all\'interno
dei multipli di $P$, che termina (per il paradosso dei compleanni) nel
$50\%$ dei casi in $O(n)$. Banalmente la contromisura è scegliere un $n$
elevato per rendere i calcoli intrattabili.

#### Baby step-Giant Step

Prese le stesse assunzioni del caso precedente, l\'obiettivo è quello di
provare i valori di sulla base della seguente formula:
$h=\lceil \sqrt{n} \thinspace \rceil c+d$ con $0\leq c,d < \lceil 
\sqrt{n} \thinspace \rceil$. Le contromisure da prendere sono le stesse
del caso precedente.

#### Silver-Pohling-Hellman

Mantenedo le stesse assunzioni dei casi precedenti, scriviamo $n$ come
$\prod_{i=1}^tq_i^{e_i}$. L\'idea è quella di trovare un
$h mod(q_i^{e_i}$ ed utilizzare il *Teorema Cinese del Resto* per
ottenere $h mod(n)$. La contromisura è quella di selezionare $n$ con un
divisore primo molto grande.

### Attacchi Specifici

#### MOV

Presi $P,Q \in E(\mathbb{F}_q)$ tali che $Q=kP$, e preso l\'ordine di
$P$, $n$ tale che $MCD(N,q)=1$. Lo scopo è quello di trovare un
isomorfismo tra $E$ e il gruppo moltiplicativo di un campo
$\mathbb{F}_q^m$, estensione di $\mathbb{F}_q$, nel quale il problema
del logaritmo discreto si risolve con un metodo subesponenziale, il
*Function Field Sieve*. Una contromisura valida è quella di verificare
che $n$ non divida $q^s-1$ per valori piccoli di $s$.

Conclusioni
===========

Abbiamo visto due metodi di approcciare la crittografia a chiave
asimmetrica, tuttavia non possiamo definire a priori un metodo superiore
all\'altro.

Sicuramente l\' ECC ha dei vantaggi notevoli:

-   è veloce nella generazione delle chiavi;
-   crea chiavi di piccole dimensioni, è quindi più facile da
    utilizzare;
-   è relativamente veloce in cifratura e decifratura;
-   può essere usata anche con altri scopi, come lo scambio delle
    chiavi.

Tuttavia la sua implementazione è difficile da realizzare in modo sicuro
in quanto ancora mancano degli standard allo stato dell\'arte, inoltre,
a causa della sua tenera età, si teme che tale famiglia di algoritmi
nasconda vulnerabilità teoriche, nonchè backdoor legate a alcune curve,
e quindi originate dalla scelta dei parametri $a$ e $b$.

Invece RSA, a discapito dei suoi aspetti negativi, legati alla
dimensione delle chiavi e alla bassa velocità per la generazione di
quest\'ultime, nonchè alla bassa velocità per le operazioni di
cifratura, è:

-   più semplice da implementare rispetto all\'ECC (anche perchè
    cifratura e decifratura sono simili);
-   più semplice da capire;
-   molto diffuso

Tuttavia, nonostante questo clima di incertezza che aleggia attorno alla
crittografia a curve ellittiche, che porterebbe a favorire RSA, queste
si stanno diffondendo abbastanza velocemente; questo è probabilmente
dovuto ai benefici quantitativi che questa tecnologia apporta. Infatti,
confrontando i benchmark di *OpenSSL* basato su RSA e della versione
basata sull\' ECC (ECDSA) si ottiene:

    Doing 256 bit sign ecdsa's for 10s: 42874 256 bit ECDSA signs in 9.99s

    Doing 2048 bit private rsa's for 10s: 1864 2048 bit private RSA's in 9.99s

ovvero in soli 9.99 secondi, la crittografia a curve ellittiche è 23
volte più veloce di RSA.

Inoltre, facendo dei confronti sulle lunghezze delle chiavi (in bit):

  RSA     ECC       ECC to RSA
  ------- --------- ------------
  1024    160-223   1:6
  2048    224-255   1:9
  3072    256-383   1:12
  7680    384-511   1:20
  15360   512+      1:30

si vede chiaramente che, a parità di sicurezza, le \"chiavi ellittiche\"
sono di gran lunga più efficenti.

Questi risultati hanno portato *Google* (e di conseguenza molti
browsers), il governo USA, *Bitcoin* (per la prova di possesso),
*iMessage*, ed altri ancora ad avvalersi di questa tecnologia. Con la
più larga adozione della ECC, sono iniziati i problemi, infatti una
vulnerabilità di un generatore di parametri per le curve ellittiche ha
permesso a gruppi di *crackers* di rompere la crittografia a protezione
di sistemi *PlayStation* e a difesa di portafogli *Bitcoin* su
dispositivi *Android*. Questi eventi, insieme a parte della teoria che
abbiamo mostrato, non fanno altro che ribadire l\'importanza della
scelta dei parametri utilizzati per generare curve ellittiche \"forti\".
Qualora questi parametri siano generati pseudocasualmente, è necessario
utilizzare programmi che abbiano un\'ottima sorgente di entropia
(problema che RSA non ha), onde evitare una vulnerabilità del
crittosistema. Infatti, negli anni, sono stati sconsigliati i programmi
che adempiono a tale scopo sviluppati da NIST e NSA, per paura di usare
inconsapevolmente curve ellittiche (\"deboli\") con la presenza di
backdoor.

Inoltre, un problema che rallenta lo sviluppo di sistemi basati sulle
curve ellittiche è che più di 130 brevetti sono detenuti da aziende e da
vari enti governativi, come l\'NSA. Questo fa si che gli sviluppatori
interrompano la loro attività di sviluppo sulle curve ellittiche, o che
siano costretti a pagare ingenti somme di denaro per utilizzare suddetti
brevetti, o ancora che si ritrovino coinvolti in una causa legale, come
la *Sony* per la causa intentata da *Certicom*.

In conclusione RSA ha superato quello che potrebbe essere definito come
il \"test del tempo\", l\' ECC tuttavia apporta notevoli benefici, ma
\"da grandi poteri derivano grandi responsabilità\".
